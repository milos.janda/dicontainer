<?php
// source: /app/config.neon

/** @noinspection PhpParamsInspection,PhpMethodMayBeStaticInspection */

declare(strict_types=1);

class Container_dcca481015 extends Nette\DI\Container
{
	protected $types = ['container' => 'Nette\DI\Container'];
	protected $aliases = [];

	protected $wiring = [
		'Nette\DI\Container' => [['container']],
		'Nette\Database\Connection' => [['01']],
		'ArticleFactory' => [['02']],
		'Controller' => [['03']],
		'UserController' => [['03']],
	];


	public function __construct(array $params = [])
	{
		parent::__construct($params);
		$this->parameters += ['db' => ['dsn' => 'sqlite::memory:', 'user' => null, 'password' => null]];
	}


	public function createService01(): Nette\Database\Connection
	{
		return new Nette\Database\Connection('sqlite::memory:', null, null);
	}


	public function createService02(): ArticleFactory
	{
		return new class ($this) implements ArticleFactory {
			private $container;


			public function __construct(Container_dcca481015 $container)
			{
				$this->container = $container;
			}


			public function create(): Article
			{
				return new Article($this->container->getService('01'));
			}
		};
	}


	public function createService03(): UserController
	{
		return new UserController($this->getService('02'));
	}


	public function createServiceContainer(): Container_dcca481015
	{
		return $this;
	}


	public function initialize()
	{
	}
}
